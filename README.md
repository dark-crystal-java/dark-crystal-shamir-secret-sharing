# JNA port of Shamirs Secret Sharing Scheme

This project is a [JNA](https://github.com/java-native-access/jna) port of
[dsprenkels/sss](https://github.com/dsprenkels/sss).

The project consists of two subprojects:
* [java](java): a Java project that runs on linux. This project can be used to
  test the JNA bindings on development machines. It contains tests, executables
  and also produces a JAR artifact that can be used in other projects.
* [android](android): an Android library project. It can be used as a module
  dependency in Android applications by including this module but it also
  produces an AAR artifact that is published to JCenter and can be used as a
  dependency in Android applications.

## Cloning this repository

This repository contains submodules. To clone it with all contained submodules
loaded properly, use this command:

    git clone --recursive git@gitlab.com:dark-crystal-java/dark-crystal-shamir-secret-sharing.git

If you do not have SSH access, use HTTPS instead:

    git clone --recursive https://gitlab.com/dark-crystal-java/dark-crystal-shamir-secret-sharing.git

If any of the submodules is missing you can clone them with this command:

    git submodule update --init --recursive
