# Android library project

## Setup

In order to work with this project, create a file `local.properties`
in the project's root directory with this content:

    sdk.dir=/path/to/Android/Sdk

where `path/to/Android/Sdk` should be replaced with an actual path to your
Android SDK. On Linux this is typically `/home/username/Android/Sdk`.

Also you need to install the appropriate SDK version, NDK and CMake
from the SDK Manager using Android Studio.

## Publishing to Maven

To upload the library part to Maven, prepare as follows.

Add the following content to your `local.properties` file mentioned above:

    local.maven.publishing.dir=file:///path/to/maven/repo

In file `libsss/build.gradle`, adapt values in the `ext` stanza.
In particular set the `libraryVersion` variable when uploading a
new version.

Then run:

    ./gradlew clean publish

Afterwards commit changes to the Maven repo git and publish to the Maven
server.
