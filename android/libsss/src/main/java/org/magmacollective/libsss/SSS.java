// Copyright 2020 Magma Collective
//
// This file is part of dark-crystal-shamir-secret-sharing.
//
// dark-crystal-shamir-secret-sharing is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// dark-crystal-shamir-secret-sharing is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dark-crystal-shamir-secret-sharing. If not,
// see <https://www.gnu.org/licenses/>.

package org.magmacollective.libsss;

import com.sun.jna.Library;
import com.sun.jna.Native;

import java.util.ArrayList;
import java.util.List;

/**
 * Shamirs secret sharing
 * @author Sebastian Kuerten
 */
public class SSS {

  public static final int KEYSHARE_LEN = 33;
  public static final int MLEN = 64;
  public static final int CLEN = MLEN + 16;
  public static final int SHARE_LEN = CLEN + KEYSHARE_LEN;
  public static final int MAX_SHARES = 255;
  public static final int KEY_LEN = 32;

  public interface LibSSS extends Library {

    LibSSS INSTANCE = Native.load("sss", LibSSS.class);

    // We would prefer to use byte[][] out and byte[][] shares, however
    // only one-dimensional arrays are supported by JNA. Hence we
    // convert between two-dimensional arrays and one-dimensional
    // arrays and vice versa between the Java and the native layer.

    void sss_create_shares(byte[] out, byte[] data, int n, int k);

    void sss_combine_shares(byte[] data, byte[] shares, int k);

    void sss_create_keyshares(byte[] out, byte[] key, int n, int k);

    void sss_combine_keyshares(byte[] key, byte[] shares, int k);
  }

  public static List<byte[]> createShares(byte[] data, int n, int k) {
    if (data.length != MLEN) throw new IllegalArgumentException("Secret must be 64 bytes long");
    byte[] flatShares = new byte[n * SHARE_LEN];
    LibSSS.INSTANCE.sss_create_shares(flatShares, data, n, k);

    List<byte[]> shares = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      byte[] share = Shares.getShare(flatShares, i);
      shares.add(share);
    }
    return shares;
  }

  public static byte[] combineShares(List<byte[]> shares)
  {
    byte[] recovered = new byte[MLEN];
    byte[] availableShares = Shares.concat(shares);
    LibSSS.INSTANCE.sss_combine_shares(recovered, availableShares, shares.size());
    return recovered;
  }

  public static List<byte[]> createKeyshares(byte[] data, int n, int k) {
    if (data.length != KEY_LEN) throw new IllegalArgumentException("Key must be 32 bytes long");
    byte[] flatShares = new byte[n * KEYSHARE_LEN];
    LibSSS.INSTANCE.sss_create_keyshares(flatShares, data, n, k);

    List<byte[]> shares = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      byte[] share = Shares.getKeyshare(flatShares, i);
      shares.add(share);
    }
    return shares;
  }

  public static byte[] combineKeyshares(List<byte[]> shares)
  {
    byte[] recovered = new byte[KEYSHARE_LEN - 1];
    byte[] availableShares = Shares.concat(shares);
    LibSSS.INSTANCE.sss_combine_keyshares(recovered, availableShares, shares.size());
    return recovered;
  }

}
