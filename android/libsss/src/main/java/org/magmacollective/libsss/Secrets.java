// Copyright 2020 Magma Collective
//
// This file is part of dark-crystal-shamir-secret-sharing.
//
// dark-crystal-shamir-secret-sharing is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// dark-crystal-shamir-secret-sharing is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dark-crystal-shamir-secret-sharing. If not,
// see <https://www.gnu.org/licenses/>.

package org.magmacollective.libsss;

/**
 * Allows a secret given as a string to be encoded as a byte array
 * @author Sebastian Kuerten
 */
public class Secrets {

  /**
   * Encode a String secret as a byte array. Uses at most {@link SSS#MLEN} bytes of the secret as
   * required by {@link SSS#createShares(byte[], int, int)}.
   */
  public static byte[] create(String secret)
  {
    byte[] data = new byte[SSS.MLEN];
    byte[] stringBytes = secret.getBytes();
    int len = Math.min(data.length, stringBytes.length);
    System.arraycopy(stringBytes, 0, data, 0, len);
    return data;
  }

}
