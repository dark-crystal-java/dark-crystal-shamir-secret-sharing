# Java project

## Development

This project can be opened in Eclipse. To do so, run:

    ./gradlew cleanEclipse eclipse

Afterwards, load the Git repository into the Git view and import the project
from there.

Alternatively, the project can be opened in Android Studio or IntelliJ.

## Building

Run this to build and test the project:

     ./gradlew test

Afterwards the test output can be inspected here:

    libsss/build/reports/tests/test/index.html

## Running executables from the command line:

There are a few executables that can be run from the command line. The project
needs to be built before like this:

    ./gradlew clean createRuntime

Afterwards you can run the following:

    ./scripts/TestSSS
    ./scripts/TestSSSEncodings

## Building Javadocs

To create the Javadocs, run this:

    ./gradlew javadoc

You should then be able to inspect the docs here:

    libsss/build/docs/javadoc/index.html

## ToDO

Which of the compiler flags from the Makefile should we port to Gradle?
Candidates:

    CFLAGS += -g -O2 -m64 -std=c99 -pedantic \
        -Wall -Wshadow -Wpointer-arith -Wcast-qual -Wformat -Wformat-security \
        -Werror=format-security -Wstrict-prototypes -Wmissing-prototypes \
        -D_FORTIFY_SOURCE=2 -fPIC -fno-strict-overflow
    hazmat.o: CFLAGS += -funroll-loops
