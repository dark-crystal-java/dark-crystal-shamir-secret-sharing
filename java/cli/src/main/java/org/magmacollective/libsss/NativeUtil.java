package org.magmacollective.libsss;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NativeUtil
{

	public static void configureJnaLibraryPath()
	{
		if (System.getProperty("cli") != null) {
			return;
		}
		Path cwd = Paths.get(System.getProperty("user.dir"));
		Path libs = cwd.resolve("../libsss/build/libs/sss/shared");
		System.setProperty("jna.library.path", libs.toString());
	}

}
