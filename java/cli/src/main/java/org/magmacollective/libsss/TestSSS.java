package org.magmacollective.libsss;

import java.util.List;

public class TestSSS
{

	public static void main(String[] args)
	{
		NativeUtil.configureJnaLibraryPath();

		int n = 3;
		int k = 2;

		byte[] data = new byte[SSS.MLEN];
		for (int i = 0; i < data.length; i++) {
			data[i] = (byte) i;
		}

		printData(data);

		List<byte[]> shares = SSS.createShares(data, n, k);

		for (byte[] share : shares) {
			printShare(share);
		}

		byte[] recovered = SSS.combineShares(shares);

		printData(recovered);
	}

	private static void printData(byte[] data)
	{
		for (int i = 0; i < data.length; i++) {
			byte b = data[i];
			System.out.printf("%02x", b);
		}
		System.out.println();
	}

	private static void printShare(byte[] share)
	{
		for (int i = 0; i < share.length; i++) {
			byte b = share[i];
			System.out.printf("%x", b);
		}
		System.out.println();
	}

}
