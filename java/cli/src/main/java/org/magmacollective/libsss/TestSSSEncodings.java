package org.magmacollective.libsss;

import java.util.List;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;

public class TestSSSEncodings
{

	public static void main(String[] args)
	{
		NativeUtil.configureJnaLibraryPath();

		int n = 3;
		int k = 2;

		byte[] data = new byte[SSS.MLEN];
		for (int i = 0; i < data.length; i++) {
			data[i] = (byte) i;
		}

		print("data", hex(data));

		List<byte[]> shares = SSS.createShares(data, n, k);

		for (byte[] share : shares) {
			print("hex", hex(share));
			print("base64", Base64.encodeBase64String(share));
			print("base32", new Base32().encodeToString(share));
			print("base32h", new Base32(true).encodeToString(share));
		}

		byte[] recovered = SSS.combineShares(shares);

		print("data", hex(recovered));
	}

	private static String hex(byte[] data)
	{
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			byte b = data[i];
			buffer.append(String.format("%02x", b));
		}
		return buffer.toString();
	}

	private static void print(String name, String encoded)
	{
		System.out.printf("%-7s (%d): %s", name, encoded.length(), encoded);
		System.out.println();
	}

}
