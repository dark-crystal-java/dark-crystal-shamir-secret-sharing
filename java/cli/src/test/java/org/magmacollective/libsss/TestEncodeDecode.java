package org.magmacollective.libsss;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestEncodeDecode
{

	@Before
	public void prepare()
	{
		NativeUtil.configureJnaLibraryPath();
	}

	@Test
	public void createShares()
	{
		byte[] secret = Secrets.create("a secret");
		List<byte[]> shares = SSS.createShares(secret, 3, 2);
		byte[] reconstructed = SSS.combineShares(shares);
		Assert.assertArrayEquals(secret, reconstructed);
	}

}
