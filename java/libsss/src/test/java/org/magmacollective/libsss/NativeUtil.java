package org.magmacollective.libsss;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NativeUtil
{

	public static void configureJnaLibraryPath()
	{
		Path cwd = Paths.get(System.getProperty("user.dir"));
		Path libs = cwd.resolve("build/libs/sss/shared");
		System.setProperty("jna.library.path", libs.toString());
	}

}
